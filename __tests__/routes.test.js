const request = require('supertest')
const app = require('../app')
const User = require('../models/User')

describe('Login', () => {
  beforeAll(async () => {
    await User.deleteMany({}) // clear DB before all tests

    const user = new User({
      email: 'tungpv@example.com',
      password: 'passWord123',
      displayName: 'Test'
    })

    await user.save()
  })

  afterAll(async () => {
    await User.deleteMany({}) // clear DB after all tests
  })

  describe('Post /login', () => {
    it('should return error: email is required', async (done) => {
      const res = await request(app)
        .post('/login')
        .send({
          password: 'passWord123',
        })
      expect(res.statusCode).toEqual(422)
      expect(res.body).toHaveProperty('message')
      done()
    })
  })

  describe('Post /login', () => {
    it('should return error: email must not be empty', async (done) => {
      const res = await request(app)
        .post('/login')
        .send({
          email: '',
          password: 'passWord123',
        })
      expect(res.statusCode).toEqual(422)
      expect(res.body).toHaveProperty('message')
      done()
    })
  })

  describe('Post /login', () => {
    it('should return error: password is required', async (done) => {
      const res = await request(app)
        .post('/login')
        .send({
          email: 'tungpv@example.com',
        })
      expect(res.statusCode).toEqual(422)
      expect(res.body).toHaveProperty('message')
      done()
    })
  })

  describe('Post /login', () => {
    it('should return error: password must not be empty', async (done) => {
      const res = await request(app)
        .post('/login')
        .send({
          email: 'tungpv@example.com',
          password: '',
        })
      expect(res.statusCode).toEqual(422)
      expect(res.body).toHaveProperty('message')
      done()
    })
  })

  describe('Post /login', () => {
    it('should return user', async (done) => {
      const res = await request(app)
        .post('/login')
        .send({
          email: 'tungpv@example.com',
          password: 'passWord123',
        })
      expect(res.statusCode).toEqual(200)
      expect(res.body).toHaveProperty('email', 'tungpv@example.com')
      done()
    })
  })
})

describe('Register', () => {
  beforeEach(async () => {
    await User.deleteMany({}) // clear DB before each test
  })

  afterEach(async () => {
    await User.deleteMany({}) // clear DB after each test
  })
  
  describe('Post /register', () => {
    it('should return error: email is required', async (done) => {
      const res = await request(app)
        .post('/register')
        .send({
          password: 'passWord123',
          displayName: 'Test2'
        })
      expect(res.statusCode).toEqual(422)
      expect(res.body).toHaveProperty('message')
      done()
    })
  })

  describe('Post /register', () => {
    it('should return error: email must not be empty', async (done) => {
      const res = await request(app)
        .post('/register')
        .send({
          email: '',
          password: 'passWord123',
          displayName: 'Test2'
        })
      expect(res.statusCode).toEqual(422)
      expect(res.body).toHaveProperty('message')
      done()
    })
  })

  describe('Post /register', () => {
    it('should return error: password is required', async (done) => {
      const res = await request(app)
        .post('/register')
        .send({
          email: 'tungpv@example.com',
          displayName: 'Test2'
        })
      expect(res.statusCode).toEqual(422)
      expect(res.body).toHaveProperty('message')
      done()
    })
  })

  describe('Post /register', () => {
    it('should return error: password must not be empty', async (done) => {
      const res = await request(app)
        .post('/register')
        .send({
          email: 'tungpv@example.com',
          password: '',
          displayName: 'Test2'
        })
      expect(res.statusCode).toEqual(422)
      expect(res.body).toHaveProperty('message')
      done()
    })
  })

  describe('Post /register', () => {
    it('should return error: displayName is required', async (done) => {
      const res = await request(app)
        .post('/register')
        .send({
          email: 'tungpv@example.com',
          password: 'passWord123',
        })
      expect(res.statusCode).toEqual(422)
      expect(res.body).toHaveProperty('message')
      done()
    })
  })

  describe('Post /register', () => {
    it('should return error: displayName must not be empty', async (done) => {
      const res = await request(app)
        .post('/register')
        .send({
          email: 'tungpv@example.com',
          password: 'passWord123',
          displayName: ''
        })
      expect(res.statusCode).toEqual(422)
      expect(res.body).toHaveProperty('message')
      done()
    })
  })

  describe('Post /register', () => {
    it('should create an user', async (done) => {
      const res = await request(app)
        .post('/register')
        .send({
          email: 'tungpv@example.com',
          password: 'passWord123',
          displayName: 'Test'
        })
      expect(res.statusCode).toEqual(200)
      expect(res.body).toHaveProperty('message', 'success')
      done()
    })
  })

  describe('Post /register', () => {
    it('should return error: duplicate record', async (done) => {
      const resCreate = await request(app)
        .post('/register')
        .send({
          email: 'tungpv@example.com',
          password: 'passWord123',
          displayName: 'Test'
        })
      expect(resCreate.statusCode).toEqual(200)
      expect(resCreate.body).toHaveProperty('message', 'success')


      const res = await request(app)
        .post('/register')
        .send({
          email: 'tungpv@example.com',
          password: 'passWord123',
          displayName: 'Test'
        })
      expect(res.statusCode).toEqual(400)
      expect(res.body).toHaveProperty('error')

      done()
    })
  })
})

